<p align="center" >
<img src="images/logo.png" alt="Highcharts" title="Highcharts">
</p>

[ ![Download](https://api.bintray.com/packages/highsoft/Highcharts/Highcharts/images/download.svg) ](https://bintray.com/highsoft/Highcharts/Highcharts/_latestVersion)

[Highcharts Android](http://www.highcharts.com/blog/mobile/) 是 Highcharts 官方发布的 Android 扩展包，这里是中文翻译及示例仓库。


# API 文档

地址： [API 文档](https://api.highcharts.com/android/highcharts/)

# 如何使用

这里我们将简单介绍 Highcharts Android 的基本使用。

## 步骤

  - 创建 Android 项目
  - 在 view  中创建图表 view
  - 创建图表配置并添加到图表 view 中
  - 运行并查看效果

## 环境准备

- 首先你需要下载 Highcharts 框架，有两种方式

**1.** 直接在 gradle 里添加依赖

在  **build.gradle**  文件里添加 Highcharts 仓库

```gradle
repositories { 
    maven { 
        url "https://highsoft.bintray.com/Highcharts" 
    }
}
```

并添加相关的依赖

```gradle
dependencies {
    compile 'com.highsoft.highcharts:highcharts:6.1r'
}
```

**2.** 直接从 https://github.com/highcharts/highcharts-android/releases 下载 _aar_  文件，并放置在  _libs_ 目录，如下图所示：

![Project structure screenshot](images/1.png "Files1")

然后在 **build.gradle** 里添加 **repositories**，代码如下：

```gradle
repositories {
    flatDir {
        dirs 'libs'
    }
}
```

并添加相关依赖

```gradle
dependencies {
    compile (name: 'highcharts-release', ext:'aar')
    compile 'com.google.code.gson:gson:2.8.0'
}
```

完成上述步骤后就可以开始使用 Highcharts 了。

## 开始使用 Highcharts（示例项目）

### 布局准备（添加 view）

首先我们需要为图表创建一个  **view**，打开  `activity_main.xml` 并添加下面打代码：

```xml
<com.highsoft.highcharts.Core.HIChartView
   android:id="@+id/hc"
   android:layout_width="match_parent"
   android:layout_height="match_parent" />

```

### 在 Activity 里添加 HIChartView

首先在 **MainActivity.java** 的顶部引入 Highcharts 相关的包

```java
import com.highsoft.highcharts.Core.*;
import com.highsoft.highcharts.Common.HIChartsClasses.*;
```

然后在 **onCreate** 方法里添加 **HIChartView**

```java
HIChartView chartView = (HIChartView) findViewById(R.id.hc); // R.id.hc 定义在上面的 activity_main 里
```

完成这个步骤后，将会根据布局里定义的大小来创建一个图表视图，现在可以开始创建图表了。

### 创建图表

为了方便演示，这里用虚拟数据创建一个简单柱形图。

图表的核心是配置，也就是 **HIOptions** 这个类，它包含所有需要配置。

创建 **HIOptions** 实例

```java
HIOptions options = new HIOptions();
```

首先我们来设置图表类型，通过创建 **HIChart** 实例并赋值，代码如下：

```java
HIChart chart = new HIChart();
chart.setType("column");
```
将 **HIChart** 实例赋值给 options

```java
options.setChart(chart);
```

然后我们来创建标题对象并赋值，同样的该对象最终也是赋值给 **options**


```java
HITitle title = new HITitle();
title.setText("Demo chart");

options.setTitle(title);
```

现在我们来给图表设置一些数据，这里创建的是柱形图，所以用 **HIColumn** 数据列。

```java
HIColumn series = new HIColumn();
```

数据是 ArrayList，通过 `setData` 来赋值给数据列

```java
series.setData(new ArrayList<>(Arrays.asList(49.9, 71.5, 106.4, 129.2, 144, 176, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4)));
```

因为图表有多个数据列，所以这里用 ArrayList 来存放数据列并赋值给 options（即数据列是个数组）。

```java
options.setSeries(new ArrayList<HISeries>(Collections.singletonList(series)));
```

最后将  **options**  赋值给视图

```java
chartView.setOptions(options);
```

到此我们已经完成了所有步骤，现在 **MainActivity.java** 的代码如下：

```java
package com.highsoft.highchartsdemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.highsoft.highcharts.Core.*;
import com.highsoft.highcharts.Common.HIChartsClasses.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        HIChartView chartView = (HIChartView) findViewById(R.id.hc);

        HIOptions options = new HIOptions();

        HIChart chart = new HIChart();
        chart.setType("column");
        options.setChart(chart);

        HITitle title = new HITitle();
        title.setText("Demo chart");

        options.setTitle(title);

        HIColumn series = new HIColumn();
        series.setData(new ArrayList<>(Arrays.asList(49.9, 71.5, 106.4, 129.2, 144, 176, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4)));
        options.setSeries(new ArrayList<HISeries>(Collections.singletonList(series)));

        chartView.setOptions(options);
    }
}
```

## 在 Android Studio 里点击 ***"Run"*** 即可看到运行结果

![示例项目运行结果](images/example.png)

#### 更多详情请参考 `Example` 里的示例项目

# 更多内容

#### 导出功能

导出共需要额外的设置，步骤如下：

1. 在 manifest 里指定 _provider_：

```xml
<provider android:authorities="com.your.package.name.FileProvider" <!-- com.your.package.name 需要修改 -->
   android:name="android.support.v4.content.FileProvider"
   android:exported="false"
   android:grantUriPermissions="true">
   <meta-data
       android:name="android.support.FILE_PROVIDER_PATHS"
       android:resource="@xml/provider_paths"/>
</provider>
```

2. 在 _res_ 下创建 _xml_ 目录并创建 _provider_paths.xml_，内容如下：

```xml
<?xml version="1.0" encoding="utf-8"?>
<paths>
   <files-path name="export" path="." />
</paths>
```

![xml folder location screenshot](images/2.png "Files2")

导出功能效果（以图表的形式分享）：

![xml folder location screenshot](images/exporting.png "Files2")


#### HIColor 示例

Highcharts Android 扩展包提供了自己的颜色实现。在图表配置里，有部分配置是 `HIColor` 类型。 你可以通过多种方法来是实例化所需的颜色（详见 [API 文档](https://api.highcharts.com/android/highcharts/)）。 在这里，我们简单说明最复杂的渐变颜色的使用。

首先需要创建  `HIGradient` 实例 :

```java
HIGradient gradient = new HIGradient(0, 0.5f, 1, 1); // 可以使用空构造函数，
```

然后定义 `HIStop`

```java
LinkedList<HIStop> stops = new LinkedList<>();
stops.add(new HIStop(0.4f, HIColor.initWithRGB(160, 160, 160)));
stops.add(new HIStop(1, HIColor.initWithRGB(60, 60, 60)));
```

有了上述两个对象，就可以实例化 **HIColor**，例如设置图表的背景色

```java
HIChart chart = new HIChart();
chart.setBackgroundColor(HIColor.initWithLinearGradient(gradient, stops));
```

#### HIFunction 示例

Highcharts Android 支持点击事件回调函数与原生程序绑定（即可以在点击事件里执行 Android原生行为）。下面示例展示了通过原生 Toast 来显示当前点击的点的坐标。

首先创建一个数据列

```java
HISpline spline = new HISpline();
spline.setData(new ArrayList<>(Arrays.asList(0.3,5.3,8.0,9.1,3.2,5.4,4.0,4.2,2.1,10.0)));
```

然后给数据点绑定点击事件及回调函数，代码如下：

```java
spline.setPoint(new HIPoint());
spline.getPoint().setEvents(new HIEvents());
spline.getPoint().getEvents().setClick(new HIFunction(
        f -> {
            Toast t = Toast.makeText(
                this,
                "Clicked point [ " + f.getProperty("x") + ", " + f.getProperty("y") + " ]",
                Toast.LENGTH_SHORT);
                t.show();
            },
            new String[] {"x", "y"}
));
```

上述代码中 `HIFunction` 中的第一个参数是 lambda 表达式定义的回调函数，第二个参数是图表中的变量数组（这些变量是当前事件对象的属性，例如 'x' 表示 point.x ），在回调函数里通过 `getProperty` 来获取这些变量。更多详情请参考 [API 文档](https://api.highcharts.com/android/highcharts/).
